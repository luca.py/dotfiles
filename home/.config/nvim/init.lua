require("config.lazy")


require("config.telescope")
require("config.nvim-tree")
require("config.startup")
require("config.bufferline")
require("config.lualine")
require("config.comment")
require("config.highlight-colors")
require("config.autopairs")
require("config.toggleterm")
require("config.treesitter")
require("config.cmp")
require("config.lspconfig")
require("config.settings")
require("config.keymaps")


-- # Colorschames
--require("config.colorschames.misery")
--require("config.colorschames.onedark")
--require("config.colorschames.catppuccin")
require("config.colorschames.decaycs")
