-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
	if vim.v.shell_error ~= 0 then
		vim.api.nvim_echo({
			{ "Failed to clone lazy.nvim:\n", "ErrorMsg" },
			{ out, "WarningMsg" },
			{ "\nPress any key to exit..." },
		}, true, {})
    vim.fn.getchar()
    os.exit(1)
	end
end
vim.opt.rtp:prepend(lazypath)

-- Make sure to setup `mapleader` and `maplocalleader` before
-- loading lazy.nvim so that mappings are correct.
-- This is also a good place to setup other settings (vim.opt)
vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

-- Setup lazy.nvim
require("lazy").setup({
	spec = {
		
		-- Colorscheme :
		{"lucauxx/misery.nvim"},
		{ "catppuccin/nvim", as = "catppuccin" },
		-- onedarkpro
		{
			"olimorris/onedarkpro.nvim",
			priority = 1000, -- Ensure it loads first
		},
		-- decaycs
		{
			"decaycs/decay.nvim",
			name = "decay",
			lazy = false,
			priority = 1000,
			config = function ()
				-- SNIP
			end
		},
		-- Fuzzy Finder (files, lsp, etc) :
		{ 
			'nvim-telescope/telescope.nvim',
			branch = '0.1.x',
			dependencies = { 
				'nvim-lua/plenary.nvim'
			} 
		},
		
		-- File tree :
		{
			"nvim-tree/nvim-tree.lua",
			version = "*",
			lazy = false,
			requires = {
				"nvim-tree/nvim-web-devicons",
			},
			opts = { on_attach = on_attach_change },
		},
		
		-- Startup :
		{
			"startup-nvim/startup.nvim",
			dependencies = { 
			"nvim-telescope/telescope.nvim", 
			"nvim-lua/plenary.nvim", 
			"nvim-telescope/telescope-file-browser.nvim"
			},
		},
		
		-- Visualize buffers as tabs :
		{
			'akinsho/bufferline.nvim', 
			version = "*", 
			dependencies = 'nvim-tree/nvim-web-devicons'
		},
		
		-- Comment code :
		{
			'numToStr/Comment.nvim',
		},

		-- Highlight Colors :
		{
			'brenoprata10/nvim-highlight-colors',
		},

		-- Autopairs :
		{
			'windwp/nvim-autopairs',
			event = "InsertEnter",
			config = true
			-- use opts = {} for passing setup options
			-- this is equivalent to setup({}) function
		},

		-- Preview markdown live in web browser :
		{
			"iamcco/markdown-preview.nvim",
			cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
			ft = { "markdown" },
			build = function() vim.fn["mkdp#util#install"]() end,
		},
		
		-- Lualine :
		{
			'nvim-lualine/lualine.nvim',
			dependencies = { 'nvim-tree/nvim-web-devicons' }
		},
		
		-- Toggleterm :
		{
			'akinsho/toggleterm.nvim', 
			version = "*", 
			config = true
		},
		
		-- Treesitter :
		{
			'nvim-treesitter/nvim-treesitter',
			lazy = true, -- Enable lazy loading
			build = ':TSUpdate', -- Use `build` instead of `do` for packer
		},

		-- LSP zero
		{
			'VonHeikemen/lsp-zero.nvim',
			branch = 'v3.x',
			lazy = true,
			config = false,
			init = function()
				-- Disable automatic setup, we are doing it manually
				vim.g.lsp_zero_extend_cmp = 0
				vim.g.lsp_zero_extend_lspconfig = 0
			end,
		},
		{
			'williamboman/mason.nvim',
			"williamboman/mason-lspconfig.nvim",
			lazy = false,
			config = true,
		},
		
		-- Autocompletion :
		{
			'hrsh7th/nvim-cmp',
			event = 'InsertEnter',
			dependencies = {
				{'L3MON4D3/LuaSnip'},
			},
		},
		
		-- LSP :
		{	
			'neovim/nvim-lspconfig',
			cmd = {'LspInfo', 'LspInstall', 'LspStart'},
			event = {'BufReadPre', 'BufNewFile'},
			dependencies = {
				{'hrsh7th/cmp-nvim-lsp'},
				{'williamboman/mason-lspconfig.nvim'},
			},
		},

	},
  -- Configure any other settings here. See the documentation for more details.
  -- colorscheme that will be used when installing plugins.
  install = { colorscheme = { "habamax" } },
  -- automatically check for plugin updates
  checker = { enabled = true },
})
