-- ~/.config/nvim/lua/config/plugins/lsp.lua

local lsp_zero = require('lsp-zero')
-- Extend lspconfig from lsp-zero
lsp_zero.extend_lspconfig()

-- On-attach function for keymaps
lsp_zero.on_attach(function(client, bufnr)
	lsp_zero.default_keymaps({ buffer = bufnr })
end)

-- Set up Mason and Mason-LSPconfig
require('mason').setup()
require('mason-lspconfig').setup({
	ensure_installed = {
		'pyright',              -- Python
	},
	handlers = {
	-- Default handler for all servers
	function(server_name)
		require('lspconfig')[server_name].setup({})
	end,

    -- Custom handler for lua_ls (Neovim-specific Lua configuration)
	lua_ls = function()
		local lua_opts = lsp_zero.nvim_lua_ls()
		require('lspconfig').lua_ls.setup(lua_opts)
    end,
	}
})

return {}
