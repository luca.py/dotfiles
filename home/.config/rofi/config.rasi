/*
* AUTHOR: Luca
*/
 
configuration{
    modi: "drun,run,window,filebrowser,emoji";
    font:				"Roboto Regular 12";
    prompt-font:        "Font Awesome 6 Free Solid 11";

    show-icons: true;
    icon-theme:"Papirus-Dark"; 
    terminal: "kitty";
    drun-display-format: "{icon} {name}";
    disable-history: false;
    hide-scrollbar: false;
    display-drun: "  ";
    display-run: "  ";
    display-emoji: " 😶 ";
    display-window: "  ";
    display-Network: " 󰤨 ";
    display-filebrowser: "  ";
    sidebar-mode: true;
    opacity: 80;
}

* {
    background:                      #1A2026;
    background-alt:                  #262F38;
	brdr:               			 #191F21;
    foreground:						 #D8D8D8;
    foresel:            			 #67AFC1;
    selected:                        #8CA1A5;
	urgent:							 #E06C75;
	on:								 #87A181;
	off:							 #D88B9F;
}

* {
    background-color:             	@background;
}


window {
    border: 						0px;
    border-color: 					@brdr;
    border-radius: 					8px;
    border-color: 					@background-alt;
    width: 							800px;
    height: 						400px;
}

prompt {
    spacing: 						0;
    border: 						0;
    border-radius: 					8px;
    background-color:             	@background-alt;
    text-color: 					@foreground;
    padding:    					6px 14px 6px 14px;

}

textbox-prompt-colon {
    expand: 						false;
    str: 							"";
    padding:    					6px 4px 6px 4px;
    text-color: 					@foreground;
    font:							"JetBrainsMono Nerd Font 10";
}

entry {
    spacing:    					0;
    expand:                         true;
    horizontal-align:               0;
    text-color: 					@foreground;
    placeholder-color:              @foreground;
    placeholder:                    "Search...";
    padding:    					6px 2px 6px 2px;
    border: 						0px 0px 0px 0px;
    border-color: 					@selected;
    border-radius: 					0px;
}

case-indicator {
    spacing:    					0;
    text-color: 					@foreground;
}

inputbar {
    spacing:    					0px;
    text-color: 					@foreground;
    border: 						0px;
    border-color: 					@selected;
    border-radius: 					0px;
    children: 						[ prompt,textbox-prompt-colon,entry ];
}

mainbox {
    border: 						0px;
    border-color: 					@selected;
    spacing: 						15px;
    padding: 						30px;
}

listview {
    lines:							4;
    columns:						2;
    fixed-height: 					0px;
    border: 						0px;
    border-color: 					@foreground;
    spacing: 						5px;
    scrollbar: 						true;
    padding: 						5px 0px 0px;
}

element-text, element-icon {
    background-color: inherit;
    text-color:       inherit;
}

element {
    border: 						0px;
    padding: 						8px;
}
element normal.normal {
    background-color: 				@background;
    text-color:       				@foreground;
}
element normal.urgent {
    background-color: 				@background;
    text-color:       				@off;
}
element normal.active {
    background-color: 				@background;
    text-color:       				@on;
}
element selected.normal {
    background-color: 				@selected;
    text-color:       				@background;
    border: 						0px;
    border-radius: 					8px;
    border-color: 					@selected;
}
element selected.urgent {
    background-color: 				@background;
    text-color:       				@off;
}
element selected.active {
    background-color: 				@background;
    text-color:       				@on;
}
element alternate.normal {
    background-color: 				@background;
    text-color:       				@foreground;
}
element alternate.urgent {
    background-color: 				@background;
    text-color:       				@foreground;
}
element alternate.active {
    background-color: 				@background;
    text-color:       				@foreground;
}

sidebar {
    border:       					0px;
    border-color: 					@selected;
    border-radius: 					0px;
}

button {
    margin: 						0px 10px 0px 10px;
    horizontal-align:               0.5;
    vertical-align:                 0.5;
    padding: 						2px;
    background-color:             	@background-alt;
    text-color: 					@background;
    border: 						0px;
    border-radius: 					8px;
    border-color: 					@selected;
}

button selected {
    background-color:             	@selected;
    text-color: 					@background;
    border: 						0px;
    border-radius: 					8px;
    border-color: 					@selected;
}

scrollbar {
    width:        					4px;
    border:       					0px;
    handle-color: 					@foreground;
    handle-width: 					8px;
    padding:      					0;
}

message {
    border: 						0px;
    border-color: 					@selected;
    padding: 						1px;
}

textbox {
    text-color: 					@foreground;
}

